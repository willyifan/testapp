//
//  ViewController.m
//  testapp
//
//  Created by Tom on 2019/3/1.
//  Copyright © 2019 Tom. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    label.center = self.view.center;
    label.text = @"测试";
    [self.view addSubview:label];
    
}


@end
